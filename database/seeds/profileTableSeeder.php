<?php

use Illuminate\Database\Seeder;

class profileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('profile')->insert([
        	'nama'=> 'udin',
        	'alamat'=> 'cianjur',
        	'kontak'=> '0000000'
        ]);

        DB::table('profile')->insert([
        	'nama'=> 'udin ganteng',
        	'alamat'=> 'cianjur timur',
        	'kontak'=> '0898989'
        ]);

        DB::table('profile')->insert([
        	'nama'=> 'udin kasep',
        	'alamat'=> 'bandung',
        	'kontak'=> '0000000'
        ]);
    }
}
